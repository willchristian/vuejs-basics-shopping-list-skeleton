var app = new Vue({
  el: '#app',
  data: {
    newItem: '',
    shoppingList: [],
  },
  methods: {
    clearAll: function () {},
    deleteItem: function (n) {},
    addItem: function () {},
    save: function () {},
  },
  mounted() {},
})
